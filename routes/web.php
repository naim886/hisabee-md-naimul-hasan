<?php

use App\Http\Controllers\AnalyticController;
use App\Http\Controllers\AnalyticImportController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('analytic-import', [AnalyticController::class, 'create'])->name('analytic.create');
Route::post('analytic-store', [AnalyticController::class, 'store'])->name('analytic.store');
Route::get('analytic-store-data', [AnalyticController::class, 'storeData'])->name('analytic.storeData');
Route::get('analytic-data/{user_id}', [AnalyticController::class, 'index'])->name('analytic.index');
Route::get('analytic-summary/{user_id}', [AnalyticController::class, 'analytic_summary'])->name('analytic.summary');
Route::get('get-user-list', [AnalyticController::class, 'user_list'])->name('user.ist');
