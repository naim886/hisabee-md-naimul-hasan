<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserAnalyticResource;
use App\Http\Resources\UserListResource;
use App\Jobs\AnalyticCSVImport;
use App\Models\Analytic;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class AnalyticController extends Controller
{
    /**
     * @return AnonymousResourceCollection
     */
    public function user_list(): AnonymousResourceCollection
    {
        $users = Analytic::select('user_id')->groupBy('user_id')->orderBy('user_id')->distinct()->get();
        return UserListResource::collection($users);
    }

    public function index($user_id, Request $request): AnonymousResourceCollection
    {

        $query = Analytic::where('user_id', $user_id);
        if ($request->input('date')) {
            $query->whereDate('created_at', $request->input('date'));
        }
        $analytics = $query->paginate(50);
        return UserAnalyticResource::collection($analytics);
    }

    /**
     * @param $user_id
     * @param Request $request
     * @return JsonResponse
     */
    public function analytic_summary($user_id, Request $request): JsonResponse
    {
        $query = Analytic::where('user_id', $user_id);
        if ($request->input('date')) {
            $query->whereDate('created_at', $request->input('date'));
        }
        $analytics = $query->count();
        return response()->json(['user_name'=>'User - '.$user_id, 'count'=>$analytics]);
    }

    /**
     * @return Factory|View|Application
     */
    public function create(): Factory|View|Application
    {
        return view('analytic_create');
    }

    /**
     * @return string
     */
    public function store(): string
    {
        if (request()->has('csv')) {
            $data = file(request()->csv);
            $chunks = array_chunk($data, 1000);
            $header = [];
            foreach ($chunks as $key => $chunk) {
                $data = array_map('str_getcsv', $chunk);
                if ($key == 0) {
                    $header = $data[0];
                    unset($data[0]);
                }
                AnalyticCSVImport::dispatch($data, $header);
            }
            return 'Data inserting on database using queue process';
        }
        return 'File not found';
    }
}
