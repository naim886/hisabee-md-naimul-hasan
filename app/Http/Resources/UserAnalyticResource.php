<?php

namespace App\Http\Resources;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property mixed $created_at
 * @property mixed $device
 * @property mixed $app_version_code
 * @property mixed $updated_at
 * @property mixed $app_platform
 * @property mixed $event
 * @property mixed $user_id
 * @property mixed $id
 */
class UserAnalyticResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request):array
    {
        return [
            'id'=> $this->id,
            'user_id'=> $this->user_id,
            'event'=> $this->event,
            'app_platform'=> $this->app_platform,
            'app_version_code'=> $this->app_version_code,
            'device'=> $this->device,
            'created_at'=> $this->created_at->toDayDateTimeString(),
            'updated_at'=>$this->created_at == $this->updated_at?'Not Updated':$this->updated_at->toDayDateTimeString(),
        ];
    }
}
