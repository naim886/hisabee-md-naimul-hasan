<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @method static create(mixed $temp_data)
 * @method static select(string $string)
 * @method static where(string $string, $user_id)
 */
class Analytic extends Model
{
    use HasFactory;
    protected  $guarded = [];
}
